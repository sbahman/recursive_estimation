function const = UnknownConstants()
% const = UnknownConstants()
% 
% Define the constants used in the simulation.  These are not accessible to
% the estimator.
%
% 
% Class:
% Introduction to Recursive Filtering and Estimation
% Spring 2016
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Markus Hehn, Max Kriegleder
%
%

%% Times
% The duration of the simulation, number of simulation steps.
const.simulationTime = 100;

% The minimum number of time samples before we get a measurement from the
% distance sensor.
const.sampleDistanceMin = 1;

% The maximum number of time samples before we get a measurement from the
% distance sensor.
const.sampleDistanceMax = 5;

% The minimum number of time samples before we get a measurement from the
% half-plane sensor.
const.sampleHalfPlaneMin = 20;

% The maximum number of time samples before we get a measurement from the
% half-plane sensor.
const.sampleHalfPlaneMax = 50;

% The minimum duration of a segment of constant control input
const.minSegTime = 5;

% The maximum duration of a segment of constant control input
const.maxSegTime = 20;

%% Control input
% The maximum value of the control input
const.MaxControlInput = 0.75;
