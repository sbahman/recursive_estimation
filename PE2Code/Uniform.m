function val = Uniform(mx)
% val = Uniform(mx)
% 
% Implements a uniform distribution between -mx and mx.
%
% 
% Class:
% Introduction to Recursive Filtering and Estimation
% Spring 2016
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Sebastian Trimpe, Markus Hehn, Max Kriegleder
%

val = -mx + 2*mx*rand;
