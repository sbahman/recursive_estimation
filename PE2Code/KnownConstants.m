function const = KnownConstants()
% const = KnownConstants()
% 
% Define the physical constants that are available to the estimator.
%
% 
% Class:
% Introduction to Recursive Filtering and Estimation
% Spring 2016
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Markus Hehn, Max Kriegleder
%
%

%% Location of horizontal distance sensor  
% Location L of distance sensor, which is at x = L, y = 0.
const.SensorLoc = 1;

%% Size of the two circles
% The radius of the upper circle, R_u.
const.radiusUpper = 0.8;

% The radius of the lower circle, R_d.
const.radiusLower = 1.4;

%% Sensor noise
% Width \bar{w} of triangular distribution of the horizontal measurement. 
const.SensorErr = 0.05;

% Probability h of a true half-plane measurement
const.HalfPlaneErr = 0.8;

%% Process noise
% Width of uniform distribution from which b and s(k) are drawn, \bar{s}.
const.MotionErr = 0.2;

%% Number of particles in the filter, N.
const.NumParticles = 500;

%% Bias of particle switching circles
% Probability of remaining on the current circle when crossing the zero
% point, p.
const.sideKeepingPreference = 0.3;


