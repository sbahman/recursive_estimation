function [input, segTime] = CalculateInputs(unknownConst)
% [input, segTime] = CalculateInputs(unknownConst)
% 
% Calculate the input for the particle motion, in rad/s, and the duration of the 
% segment in seconds.  This is only called during the simulation; not from
% the estimator.
%
% In this implementation, the inputs are piecewise constant functions of 
% time, that is for a segment of random length (segTime), the input is constant.
%
% Input:
%   unknownConst    constants not known to the estimator (from UnknownConstants.m)
%
% Output:
%   input           angular velocity command (in rad/s)
%   segTime         duration of segment (in s) for that the inputs are
%                   constant
%
%
% Class:
% Recursive Estimation
% Spring 2016
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Markus Hehn, Max Kriegleder
%
    
% The length of the segment, in seconds.
% Draw from uniform distribution.
segTime = UniformMinMax(unknownConst.minSegTime,unknownConst.maxSegTime);

% The control input. Draw from a uniform distribution.
input = Uniform(unknownConst.MaxControlInput);
