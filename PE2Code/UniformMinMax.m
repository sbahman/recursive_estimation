function val = UniformMinMax(mn,mx)
% val = UniformMinMax(mn,mx)
% 
% Implements a uniform distribution between mn and mx.
%
% 
% Class:
% Introduction to Recursive Filtering and Estimation
% Spring 2016
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Sebastian Trimpe, Markus Hehn, Max Kriegleder
%

val = mn + (mx-mn)*rand;
