function [posEst,oriEst,radiusEst, posVar,oriVar,radiusVar,estState] = Estimator(estState,actuate,sense,tm,estConst)
% [posEst,oriEst,posVar,oriVar,baseEst,baseVar,estState] =
% 	Estimator(estState,actuate,sense,tm,knownConst,designPart)
%
% The estimator.
%
% The function will be called in two different modes:
% If tm==0, the estimator is initialized; otherwise the estimator does an
% iteration step (compute estimates for the time step k).
%
% Inputs:
%   estState        previous estimator state (time step k-1)
%                   May be defined by the user (for example as a struct).
%   actuate         control input u(k), [1x2]-vector
%                   actuate(1): u_V, drive wheel angular velocity
%                   actuate(2): u_R, drive wheel angle
%   sense           sensor measurements z(k), [1x2]-vector, INF if no
%                   measurement
%                   sense(1): z_d, distance measurement
%                   sense(2): z_r, orientation measurement
%   tm              time, scalar
%                   If tm==0 initialization, otherwise estimator
%                   iteration step.
%   estConst        estimator constants (as in EstimatorConstants.m)
%
% Outputs:
%   posEst          position estimate (time step k), [1x2]-vector
%                   posEst(1): x position estimate
%                   posEst(2): y position estimate
%   oriEst          orientation estimate (time step k), scalar
%   radiusEst       estimate of wheel radius W (time step k), scalar
%   posVar          variance of position estimate (time step k), [1x2]-vector
%                   posVar(1): x position variance
%                   posVar(2): y position variance
%   oriVar          variance of orientation estimate (time step k), scalar
%   radiusVar       variance of wheel radius estimate (time step k), scalar
%   estState        current estimator state (time step k)
%                   Will be input to this function at the next call.
%
%
% Class:
% Recursive Estimation
% Spring 2016
% Programming Exercise 1
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Michael Muehlebach
% michaemu@ethz.ch
%
% --
% Revision history
% [19.04.11, ST]    first version by Sebastian Trimpe
% [30.04.12, PR]    adapted version for spring 2012, added unknown wheel
%                   radius
% [06.05.13, MH]    2013 version
% [23.04.15, MM]    2015 version
% [14.04.16, MM]    2016 version


%% Mode 1: Initialization
if (tm == 0)
    % Do the initialization of your estimator here!
    x_0 = [0;0;0;estConst.NominalWheelRadius];
    P_0 = diag ([ estConst.TranslationStartBound^2/3,   estConst.TranslationStartBound^2/3 , ...
        estConst.RotationStartBound^2/3 , (estConst.WheelRadiusError^2)/3 ]); 
    
    estState.Pm = P_0;
    estState.xm = x_0;
    estState.ltm = tm;  
    
    % Replace the following:
    posEst = x_0(1:2)';
    oriEst = x_0(3);
    posVar = [P_0(1,1) , P_0(2,2)];
    oriVar = P_0(3,3);
    radiusEst = x_0(4);
    radiusVar = P_0(4,4);
    return;
end


%% Mode 2: Estimator iteration.
% If we get this far tm is not equal to zero, and we are no longer
% initializing.  Run the estimator.

% differential eq. functions
function nx = gl(t, x)
    nx = zeros(4,1);
    nx(1) = x(4)*actuate(1)*(1 + 0)*cos(actuate(2)+0)*cos(x(3));
    nx(2) = x(4)*actuate(1)*(1 + 0)*cos(actuate(2)+0)*sin(x(3));
    nx(3) = -x(4)*actuate(1)*(1 + 0)*sin(actuate(2)+0)/estConst.WheelBase;
    nx(4) = 0;
end

 function Pdot = diff(t, P)
    Pdot = zeros(4);
    Pdot = At*P + P*A' + Lt*Q*Lt';
 end
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if (sense ~= [Inf,Inf] )
    
    % prior update of xp and Pp
    x_prev = estState.xm;
    
    [T,X] = ode45(@gl, [estState.ltm,tm], estState.xm);
    xp = X(end,:)';
    
    At = [0 , 0 , - x_prev(4)*actuate(1)*(1 + 0)*cos(actuate(2)+0)*sin(x_prev(3)) ,actuate(1)*(1 + 0)*cos(actuate(2)+0)*cos(x_prev(3)) ;...
          0 , 0 ,  x_prev(4)*actuate(1)*(1 + 0)*cos(actuate(2)+0)*cos(x_prev(3)) , actuate(1)*(1 + 0)*cos(actuate(2)+0)*sin(x_prev(3)) ; ...
          0 , 0,                       0       ,  - actuate(1)*(1 + 0)*sin(actuate(2)+0)/estConst.WheelBase         ;...
          0 ,0  , 0, 0];
      
    Lt = [];
    
    Q  = diag( [ estConst.AngleInputPSD , estConst.VelocityInputPSD ]); % Q_r , Q_v
      
    
    [T,P] = ode45(@diff, [estState.ltm,tm], estState.Pm);
    Pp = P(end,:,:)';
    
    % measurement update 
    
       

end



% Replace the following:
posEst = estState.xm(1:2)';
oriEst = estState.xm(3);
posVar = [estState.Pm(1,1) , estState.Pm(2,2)];
oriVar = estState.Pm(3,3);
radiusEst = estState.xm(4);
radiusVar = estState.Pm(4,4);
end










